# recreate-systemfolders

Script to re-create system folders. 

**Note: Inbox can't be re-created**

Supported folders
- Calendar
- Contacts
- Drafts
- Journal
- Notes
- Outbox
- Sent Items
- Deleted Items
- Junk E-mail

**Usage**
```python
python3 recreate-systemfolders.py --user username --create foldername --systemfolder systemfoldername**
```
List of system folder parameter names:

- calendar
- contacts
- drafts
- journal
- notes
- outbox
- sentmail
- tasks
- wastebasket
- junk
